@echo off
color 00

SET user_name=Volodymyr L
SET user_email=60s24h@gmail.com
SET https_git=https://gitlab.com/capup/eplan_new.git
SET main_branch=master
SET develop_branch=develop

echo:Is is correct?
echo:user: %user_name%
echo:email: %user_email%
echo:https: %https_git%
echo:...(Press ``Enter``)
pause > nul
echo:
git --version
git config --global user.name %user_name%
git config --global user.email %user_email%
git init
git remote add origin %https_git%
git add .
git status
echo:
SET /P memo = "Add comment to commit: "
IF "%memo%"=="" (
    SET memo=:memo: %date%_%time%
)
echo:

SET /P branch = "Branch name is ('master'): "
IF "%branch%"=="" (
    SET branch=master
)
git branch %branch%

git commit -m ":memo: %memo%"

echo:
git push --set-upstream origin %branch%
rem start "" "%https_git%"
echo:
echo:... (git push is finised! Press ``Enter``)
pause > nul