from django.db import models


class UnregisteredUser(models.Model):
    name = models.CharField(max_length=20)
    #web_site = models.URLField(max_length=255, blank=True, null=True)
    email = models.EmailField()
    ip = models.GenericIPAddressField(blank=True, null=True)

    def __str__(self):
        return self.name 