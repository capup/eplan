from django.shortcuts import render
from django.http import HttpRequest, HttpResponse
from django.shortcuts import get_object_or_404
from django.conf import settings
from django.db import models



def test1(request: HttpRequest) -> HttpResponse:
    user = request.user

    context = {

        "welcome_message": f"It is template! Hello, {user}!",

    }

    return render(request, "core/test.html", context=context)


def test2(request: HttpRequest) -> HttpResponse:
    user = request.user.username
    return HttpResponse(user)
