from django.contrib import admin

from .models import UnregisteredUser


@admin.register(UnregisteredUser)
class UnregisteredUserAdmin(admin.ModelAdmin):
    list_display = ("name", "email", "ip")

