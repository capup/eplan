from django.shortcuts import render
from django.http import HttpRequest, HttpResponse
from django.shortcuts import get_object_or_404
from django.conf import settings
from django.db import models
from .models import Event
from django.contrib.auth import (
    REDIRECT_FIELD_NAME, get_user_model, login as auth_login,
    logout as auth_logout, update_session_auth_hash,
)


def homepage(request: HttpRequest) -> HttpResponse:
    user = request.user
    context = {
        "welcome_message": f"It is template! Hello, {user}!",
    }
    return render(request, "event/test.html", context=context)


def table(request: HttpRequest) -> HttpResponse:
    user = request.user
    context = {}
    all_events = Event.objects.all
    # all_events = Event.objects.filter(is_event_name = True, event_name_contains = 'Чемпионат')
    # print(all_events)
    # for event in all_events:
    # print(event.name_event)
    context['events'] = all_events
    #return HttpResponse(context['events'])


    return render(request, "event/events.html", context=context)