from django.utils import timezone
from django.http import HttpRequest, HttpResponse
from django.contrib.auth import get_user_model
from django.db import models
# from django_quill.fields import QuillField
from django.utils.text import slugify
from django.conf import settings

# from core.models import UnregisteredUser


User = get_user_model()

"""
user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        models.CASCADE,
        verbose_name=('user'),
    )
"""

"""
DepartmentList
OrganizationTypeList
PositionList
GroupList
"""


class AwardsList(models.Model):
    awards = models.CharField(verbose_name="Нагороди", max_length=32)

    def __str__(self):
        return self.awards

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

    class Meta:
        verbose_name_plural = "Нагороди"


class AudienceList(models.Model):
    audience = models.CharField(verbose_name="Цільова аудиторія", max_length=32)

    def __str__(self):
        return self.audience

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

    class Meta:
        verbose_name_plural = "Цільові аудиторія"


class DepartmentList(models.Model):
    department = models.CharField(verbose_name="Відділ", max_length=32)

    def __str__(self):
        return self.department

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

    class Meta:
        verbose_name_plural = "Відділи"


class OrganizationTypeList(models.Model):
    organization_type = models.CharField(verbose_name="Тип організації", max_length=32)

    def __str__(self):
        return self.organization_type

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

    class Meta:
        verbose_name_plural = ("Типи організацій")


class PositionList(models.Model):
    position = models.CharField(verbose_name="Посада", max_length=32)

    def __str__(self):
        return self.position

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

    class Meta:
        verbose_name_plural = ("Посади")


class GroupList(models.Model):
    group = models.CharField(verbose_name="Назва групи", max_length=32)

    def __str__(self):
        return self.group

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

    class Meta:
        verbose_name_plural = ("Групи")


class UserMeta(models.Model):
    user = models.ForeignKey(
        User, verbose_name="Логін", on_delete=models.CASCADE, related_name="meta")
    slug = models.SlugField(max_length=64, default="", editable=False)
    phone1 = models.CharField(verbose_name="Телефон#1", max_length=12)
    phone2 = models.CharField(verbose_name="Телефон#2", max_length=12)
    group = models.ForeignKey(
        GroupList, verbose_name="Група", on_delete=models.CASCADE, blank=False)
    position = models.ForeignKey(PositionList, verbose_name="Посада", on_delete=models.CASCADE, blank=False)
    organization_type = models.ForeignKey(
        OrganizationTypeList, verbose_name="Тип організації", on_delete=models.CASCADE, blank=False)
    organization_name = models.CharField(verbose_name="Назва організації", max_length=64)
    department = models.ForeignKey(
        DepartmentList, verbose_name="Відділ", on_delete=models.CASCADE, blank=False)
    facebook = models.URLField(verbose_name="facebook")
    person_name = models.CharField(max_length=32, verbose_name="Ім'я")
    person_midname = models.CharField(max_length=32, verbose_name="По - батькові")
    person_surname = models.CharField(max_length=32, verbose_name="Прізвище")
    email = models.EmailField(max_length=254, verbose_name="Пошта")

    def __str__(self):
        return self.user

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

    class Meta:
        verbose_name_plural = ("Користувач")


class DirectList(models.Model):
    direct = models.CharField(max_length=32)

    def __str__(self):
        return self.direct

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

    class Meta:
        verbose_name_plural = ("Напрям діяльності")


class FinancingList(models.Model):
    financing = models.CharField(max_length=32)

    def __str__(self):
        return self.financing

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

    class Meta:
        verbose_name_plural = ("Джерела фінансування")


class EngagementList(models.Model):
    engagement = models.CharField(max_length=32)

    def __str__(self):
        return self.engagement

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

    class Meta:
        verbose_name_plural = ("Ролі в організації заходу")


class PlaceList(models.Model):
    place = models.CharField(max_length=32)

    def __str__(self):
        return self.place

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

    class Meta:
        verbose_name_plural = ("Місця проведення заходів")


class EventTypeList(models.Model):
    event_type = models.CharField(max_length=32)

    def __str__(self):
        return self.event_type

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

    class Meta:
        verbose_name_plural = ("Типи заходів")


class StatusList(models.Model):
    DEFAULT_STATUS = 1
    status = models.CharField(max_length=16, default="", blank=False)
    slug = models.CharField(max_length=16, default="", editable=False)

    def __str__(self):
        return self.status

    @classmethod
    def _get_slug(cls, status):
        status_slug = slugify(status)
        count_with_same_slug = cls.objects.filter(slug=status_slug).count()

        if count_with_same_slug is not 0:
            return f"{status_slug}-{count_with_same_slug}"

        return status_slug

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = self._get_slug(self.status)
        super().save(*args, **kwargs)

    class Meta:
        verbose_name_plural = ("Статуси")


class TitleList(models.Model):
    # id
    title = models.CharField(max_length=512, default="", blank=False)
    slug = models.CharField(max_length=512, default="", editable=False)

    def __str__(self):
        return self.title

    @classmethod
    def _get_slug(cls, title):
        title_slug = slugify(title)
        count_with_same_slug = cls.objects.filter(slug=title_slug).count()

        if count_with_same_slug is not 0:
            return f"{title_slug}-{count_with_same_slug}"

        return title_slug

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = self._get_slug(self.title)
        super().save(*args, **kwargs)

    class Meta:
        verbose_name_plural = ("Назви")


class Event(models.Model):
    slug = models.SlugField(max_length=512, default="", editable=False)
    editor = models.CharField(max_length=64, default=User, editable=False)
    image = models.ImageField(upload_to='uploads/%Y/%m/%d/')
    status = models.ForeignKey(
        StatusList, on_delete=models.CASCADE, blank=False, default=StatusList.DEFAULT_STATUS, verbose_name="Статус")
    title = models.ForeignKey(TitleList, on_delete=models.CASCADE, blank=False, verbose_name="Назва заходу")
    datetime_creation = models.DateTimeField(verbose_name="Дата створення", auto_now_add=True)
    datetime_edit = models.DateTimeField(verbose_name="Остання зміна", auto_now=True)
    event_type = models.ForeignKey(EventTypeList, on_delete=models.CASCADE, blank=False, verbose_name="Тип заходу")
    event_direct = models.ForeignKey(DirectList, on_delete=models.CASCADE, blank=False,
                                     verbose_name="Напрям діяльності")
    department = models.CharField(verbose_name="Вшдділ", max_length=64, default=User, editable=False)
    financing = models.ForeignKey(FinancingList, on_delete=models.CASCADE, blank=False,
                                  verbose_name="Джерело фінансування")
    price = models.IntegerField(verbose_name="Сума кошторису", default=0)
    engagement = models.ForeignKey(EngagementList, verbose_name="Роль в організації", on_delete=models.CASCADE,
                                   blank=False)
    datetime_start = models.DateTimeField(verbose_name="Початок заходу", blank=False,
                                          default=timezone.now)  # default=auto_now_add
    datetime_end = models.DateTimeField(verbose_name="Закінчення заходу", blank=False,
                                        default=timezone.now)  # default=auto_now_add
    place = models.ForeignKey(PlaceList, verbose_name="Місце проведення заходу", on_delete=models.CASCADE, blank=False)
    # partners = models.ManyToManyField()
    # respons_person = models.ManyToManyField()
    plan_out = models.BooleanField(verbose_name="Позаплановий")
    nation = models.BooleanField(verbose_name="Національно - патриотичне виховання")
    imap_aid = models.BooleanField(verbose_name="Допомога ІМАП")
    vxt_aid = models.BooleanField(verbose_name="Допомога ВХТ")
    music_aid = models.BooleanField(verbose_name="Музичний супровід")
    mic_aid = models.BooleanField(verbose_name="Мікрофони/підсилювачі")
    is_edit = models.BooleanField(verbose_name="Дозволено редагування")

    audience = models.ManyToManyField(AudienceList, verbose_name="Цільова аудиторія")
    part_num_inter = models.PositiveIntegerField(verbose_name="Кількість учасників КПДЮ", default=0)
    part_num_exter = models.PositiveIntegerField(verbose_name="Кількість сторонніх учасників", default=0)
    achievement = models.TextField(verbose_name="Коментар", max_length=1024)

    awards = models.ManyToManyField(AwardsList, verbose_name="Нагороди")
    document1 = models.FileField(verbose_name="Наказ", upload_to='uploads/%Y/%m/%d/')
    document2 = models.FileField(verbose_name="Кошторис", upload_to='uploads/%Y/%m/%d/')
    document3 = models.FileField(verbose_name="Список", upload_to='uploads/%Y/%m/%d/')

    image1 = models.ImageField(verbose_name="Загальне фото", upload_to='uploads/%Y/%m/%d/',  blank=True)
    image2 = models.ImageField(verbose_name="Фото заходу", upload_to='uploads/%Y/%m/%d/', blank=True)
    image3 = models.ImageField(verbose_name="Фото нагородження", upload_to='uploads/%Y/%m/%d/', blank=True)

    link1 = models.CharField(verbose_name="Посилання #1", max_length=512, blank=True)
    link2 = models.CharField(verbose_name="Посилання #2", max_length=512, blank=True)
    link3 = models.CharField(verbose_name="Посилання #3", max_length=512, blank=True)

    comments = models.TextField(verbose_name="Коментар", max_length=512, blank=True)

    points = models.PositiveIntegerField(verbose_name="Оцінка якості", default=0)

    class Meta:
        verbose_name_plural = ("Заходи")

    @classmethod
    def _get_slug(cls, title):
        title_slug = slugify(title)
        count_with_same_slug = cls.objects.filter(slug=title_slug).count()

        if count_with_same_slug is not 0:
            return f"{title_slug}-{count_with_same_slug}"

        return title_slug

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = self._get_slug(self.title)
        super().save(*args, **kwargs)

    def __str__(self):
        return self.title.title
