from django.contrib import admin
from .models import TitleList, StatusList, Event, \
    EventTypeList, DirectList, EngagementList, PlaceList, \
    FinancingList, UserMeta, AudienceList, AwardsList


@admin.register(AwardsList)
class AwardsListAdmin(admin.ModelAdmin):
    list_display = ('awards',)


@admin.register(AudienceList)
class AudienceListAdmin(admin.ModelAdmin):
    list_display = ('audience',)


@admin.register(UserMeta)
class UserMetaAdmin(admin.ModelAdmin):
    list_display = ('phone1', 'user')


@admin.register(FinancingList)
class FinancingListAdmin(admin.ModelAdmin):
    list_display = ('financing',)


@admin.register(PlaceList)
class PlaceListAdmin(admin.ModelAdmin):
    list_display = ('place',)


@admin.register(DirectList)
class DirectListAdmin(admin.ModelAdmin):
    list_display = ('direct',)


@admin.register(EngagementList)
class EngagementListAdmin(admin.ModelAdmin):
    list_display = ('engagement',)


@admin.register(EventTypeList)
class EventTypeAdmin(admin.ModelAdmin):
    list_display = ('event_type',)


@admin.register(TitleList)
class TitleListAdmin(admin.ModelAdmin):
    list_display = ('title',)


@admin.register(StatusList)
class StatusListAdmin(admin.ModelAdmin):
    list_display = ('status',)


@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
    list_display = ("status", "title", "slug")
